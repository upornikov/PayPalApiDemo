# Paypal Adaptive Payments Demo
A bank integration prototype for proprietary REST API around various PayPal APIs. It operates through PayPal sandbox: to see payment simulations in action, a customer must have a valid PayPal sandbox login (email) - corresponding transactions will be reflected in his account.

# Dependencies: 
 - Python27, 
 - jinja2, 
 - Google AppEngine framework.


# Try live demos:
There are featured applications in the Google cloud, where you can see how this works: http://qpptest.appspot.com   -  which hosts 3 applications to demo different PayPal APIs:
 - http://qpptest.appspot.com/accountpayment   -  PayPal account payment demo;
 - http://qpptest.appspot.com/cardpayment   -  PayPal cart payment demo;
 - http://qpptest.appspot.com/merchant   -  New merchant registration demo;


# The demo features:
 - Create new customer by sandbox PayPal login;
 - Delete custormer;
 - Create/Delete sandbox payment pre-approvals;
 - Money transfer between sandbox customers' accounts;
 - PayPal card payment;
 - Merchant authentication;
 - Direct card payment;
 - List payments;
 - Refund payment;


