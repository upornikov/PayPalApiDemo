from collections import OrderedDict
import urllib, urllib2

class PayPalAdaptivePaymentsProcessor(object):
    #in AdaptivePayments model 3 parties: handler(payment processor), sender(buyer), receiver(merchant)
    class ReceiverList(object):
        """ReceiverList({'email':'receiver@dot.com', 'amount':'0.50', 'primary':'false'}, {...}, ...)"""
        def __init__(self, *arg):
            self.receiverList = arg
        def flatten(self):
            res = OrderedDict()
            for (i,r) in enumerate(self.receiverList):
                for k in r: res['receiverList.receiver(%d).%s'%(i,k)] = r[k]
            return res
        def __str__(self):
            return str(self.receiverList)
    def importCredentials(self):
        self.sandboxed = False
        self.sandbox_prefix = '/sandbox'
        if self.hostUrl[:8].lower() == self.sandbox_prefix:
            self.sandboxed = True
        else:
            self.sandbox_prefix = ''
        if self.sandboxed:        
            from pamcredssandbox import adaptivePaymentsApiRootUrl, pamApiHandlerId, pamApiHandlerPwd, pamApiHandlerSig, pamApiHandlerAppId
        else:
            from pamcreds import adaptivePaymentsApiRootUrl, pamApiHandlerId, pamApiHandlerPwd, pamApiHandlerSig, pamApiHandlerAppId
        self.apiRootUrl = adaptivePaymentsApiRootUrl 
        self.handlerId = pamApiHandlerId
        self.handlerPwd = pamApiHandlerPwd
        self.handlerSig = pamApiHandlerSig
        self.appId = pamApiHandlerAppId
    
    def __init__(self, hostUrl):        
        self.hostUrl = hostUrl
        self.importCredentials()
        self.cancelledUrl = hostUrl + '/cancelled'
        self.completedUrl = hostUrl + '/completed'
        self.headers = {
            'Content-type':'application/x-www-form-urlencoded',
            'Accept':'text/plain',
            'X-PAYPAL-SECURITY-USERID':self.handlerId, 
            'X-PAYPAL-SECURITY-PASSWORD':self.handlerPwd, 
            'X-PAYPAL-SECURITY-SIGNATURE':self.handlerSig,
            'X-PAYPAL-APPLICATION-ID':self.appId,
            'X-PAYPAL-REQUEST-DATA-FORMAT':'NV',
            'X-PAYPAL-RESPONSE-DATA-FORMAT':'NV'
            }
        self.envelope = {
            'requestEnvelope.errorLanguage':'en_US',
            'requestEnvelope.detailLevel':'ReturnAll',
            }
    def call_api(self, func, data):
        jsondata = urllib.urlencode(data)
        req = urllib2.Request(self.apiRootUrl + func, jsondata)
        req.add_header('Content-Length', len(jsondata))
        for k in self.headers:
            req.add_header(k, self.headers[k])
        res = OrderedDict() 
        rs = ''        
        try:
            response = urllib2.urlopen(req)
            rs = response.read()
        except:
            return res
        rd = urllib2.urlparse.parse_qs(rs)
        for k in rd: res[k] = rd[k][0]
        return res
    def create_payment(self, senderId, senderEmail, receiverList, currencyCode, paymentId, 
                       feesPayer='EACHRECEIVER', memo=None,
                       preapprovalKey=None, pin=None, 
                       cancelUrl = None, returnUrl = None, ipnNotificationUrl = None):
        data = OrderedDict()
        data.update(self.envelope)
#        data.update(receiverList.flatten())
        data.update(receiverList)
        data.update({'actionType':'PAY',
                     'currencyCode':currencyCode,
                     'feesPayer':feesPayer })
        if senderEmail: data['senderEmail'] = senderEmail
        if memo: data['memo'] = memo
        if preapprovalKey: data['preapprovalKey'] = preapprovalKey
        if pin: data['pin'] = pin
        queryCancelUrl = {'id': paymentId, 'url': cancelUrl or self.cancelledUrl}
        queryReturnUrl = {'id': paymentId, 'url': returnUrl or self.completedUrl}
        queryIpnNotificationUrl = {'id': paymentId, 'url': ipnNotificationUrl or self.completedUrl}
        data['cancelUrl'] = '%s/commit/oncancel?%s'%(self.hostUrl, urllib.urlencode(queryCancelUrl))
        data['returnUrl'] = '%s/commit/onreturn/?%s'%(self.hostUrl, urllib.urlencode(queryReturnUrl))
        data['ipnNotificationUrl'] = '%s/commit/onipnnotificaiton?%s'%(self.hostUrl, urllib.urlencode(queryIpnNotificationUrl))
        return self.call_api('Pay', data)
    def create_preapproval(self, preapprovalId, *k, **kw):
        data = {}
        data.update(self.envelope)
        data.update(kw)
        queryCancelUrl = {'id': preapprovalId, 'url': data.get('cancelUrl') or self.cancelledUrl}
        queryReturnUrl = {'id': preapprovalId, 'url': data.get('returnUrl') or self.completedUrl}
        queryIpnNotificationUrl = {'id': preapprovalId, 'url': data.get('ipnNotificationUrl') or self.completedUrl}
        data['cancelUrl'] = '%s/preapproval/oncancel?%s'%(self.hostUrl, urllib.urlencode(queryCancelUrl))
        data['returnUrl'] = '%s/preapproval/onreturn/?%s'%(self.hostUrl, urllib.urlencode(queryReturnUrl))
        data['ipnNotificationUrl'] = '%s/preapproval/onipnnotificaiton?%s'%(self.hostUrl, urllib.urlencode(queryIpnNotificationUrl))
        return self.call_api('Preapproval', data)
    def cancel_preapproval(self, preapprovalKey):
        data = {}
        data.update(self.envelope)
        data.update({'preapprovalKey':preapprovalKey})
        return self.call_api('CancelPreapproval', data)
    def do_refund(self, **kw):
        data = {}
        data.update(self.envelope)
        data.update(kw)
        return self.call_api('Refund', data)
