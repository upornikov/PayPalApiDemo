from google.appengine.ext import db

'''PAM data structures'''

class User(db.Model):
    '''POS operator data class'''
    username = db.StringProperty(required = True)
    passwordhash = db.StringProperty(required = True)
    created = db.DateTimeProperty(auto_now_add = True)

class Customer(db.Model):
    '''Service customer data class'''
    nick = db.StringProperty(required = True)
    ppLogin = db.StringProperty(required = True)
    preapproval = db.StringProperty(required = False)
    
class Ledger(db.Model):
    '''Service ledger data class'''
    paymentId = db.StringProperty(required = True)
    senderId = db.StringProperty(required = True)
    senderEmail = db.StringProperty(required = True)
    payKey = db.StringProperty(required = True)
    currencyCode = db.StringProperty(required = True)
    created = db.DateTimeProperty(auto_now_add = True)
