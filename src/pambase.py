import webapp2
import datetime, random, string, hmac, time
from google.appengine.ext import db
from google.appengine.api import memcache


class TheSecretHandler():
    '''Handler provides static methods to enable PAM user level protection'''
    secret = "rKZ4loiWWvGhBiDw"
    @staticmethod
    def make_salt(size = 5):
        return ''.join(random.choice(string.letters) for i in range(0,size))
    @classmethod
    def make_password_hash(cls, password, salt = None):  
        if not salt: 
            salt = cls.make_salt()
        return '%s|%s' % (hmac.new(cls.secret, password + salt).hexdigest(), salt)
    @classmethod
    def check_password(cls, password, passwordhash):
        try:
            return passwordhash == cls.make_password_hash(password, passwordhash.split('|')[1])
        except:
            return False
    @classmethod
    def make_secure_cookie(cls, value, salt = None):
        if not salt: 
            salt = cls.make_salt()
        return '%s|%s|%s' % (value, hmac.new(cls.secret, str(value) + salt).hexdigest(), salt)
    @classmethod
    def check_secure_cookie(cls, cookie):
        try:
            val, h, salt = cookie.split('|')
            return cookie == cls.make_secure_cookie(val, salt)
        except:
            return False


class TheRequestHandler(webapp2.RequestHandler):
    '''PAM superclass request handler, which enables PAM UI login and provides db query caching'''
    def checkSandboxMode(self):
        self.sandboxed = False
        self.sandbox_prefix = '/sandbox'
        if self.request.path.lower()[:8] == self.sandbox_prefix:
            self.sandboxed = True
        else:
            self.sandbox_prefix = ''
    def importDb(self):
        '''Import sandbox/retail PAM client (sale at LOM)'''
        if self.sandboxed:
            from pamdatasandbox import SandboxUser as theUser, SandboxCustomer as theCustomer, SandboxLedger as theLedger 
        else:            
            from pamdata import User as theUser, Customer as theCustomer, Ledger as theLedger
        return theUser, theCustomer, theLedger
    def initialize(self, *a, **kw):
        super(TheRequestHandler, self).initialize(*a, **kw)
        self.checkSandboxMode()
        self.userDb, self.customerDb, self.ledgerDb = self.importDb()  
        uid = self.read_secure_cookie('user_id')
        self.user = uid and self.userDb.get_by_id(int(uid))
        if  self.user:
            self.login = self.user.username
        else:
            self.login = 'anonymous'
    def read_secure_cookie(self, name):
        cookie = self.request.cookies.get(name)
        return cookie and TheSecretHandler.check_secure_cookie(cookie) and cookie.split('|')[0]
    def set_secure_cookie(self, name, value, minutesToLive):
        expire = datetime.datetime.utcnow() + datetime.timedelta(minutes=minutesToLive)
        self.response.headers.add_header('Set-Cookie', '%s=%s; expires=%s; Path=/'%(name, value, expire.strftime("%a, %d %b %Y %H:%M:%S GMT")))
    def cache_clean(self):
        memcache.flush_all()
    def cache_query(self, query, update = False):
        val = None
        if not update:
            val = memcache.get(query)
        if not val:
            rep = list(db.GqlQuery(query))
            val = (time.time(), rep)
            memcache.set(query, val)
        return val
