#Service subscription code provides a proof-of-concept and should be used in a sandbox only

from pambase import TheRequestHandler
from google.appengine.api import urlfetch
import os, jinja2, logging, time
import urllib, urllib2, cgi


template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir), autoescape = True)


class DemoSubscriptionHandler(TheRequestHandler):
    '''Handler provides demo implementation of a Service subscription functionality'''
    def render(self, **kwargs):
        t = jinja_env.get_template('customerRegistrationForm.html')
        self.response.out.write(t.render(kwargs))
    def __init__(self, request, response):
        self.initialize(request, response)
        self.payUrl = self.request.host_url + '%s/accountpayment/commit'%self.sandbox_prefix
        self.newCustomerUrl = self.request.host_url + '%s/accountpayment/customer/create'%self.sandbox_prefix
        self.newPreapprovalUrl = self.request.host_url + '%s/accountpayment/preapproval/create'%self.sandbox_prefix
        self.hostUrl = self.request.host_url + '%s/subscribe'%self.sandbox_prefix
        self.confirmUrl = self.hostUrl + '/confirm'
        self.cancelUrl = self.hostUrl + '/cancel'
        self.cancelledUrl = self.request.host_url + '%s/accountpayment/cancelled'%self.sandbox_prefix
        self.completedUrl = self.request.host_url + '%s/accountpayment/completed'%self.sandbox_prefix
    def get(self, func = ''):
        self.request.headers['Content-Type'] = 'text/html'
        if func == '':
            if not self.user: return self.redirect('%s/login'%self.sandbox_prefix)
            mode = 'init'
            return self.render(mode=mode, sandboxPrefix=self.sandbox_prefix)
        elif func == 'confirm':
            mode = 'confirm'
            return self.render(mode=mode, sandboxPrefix=self.sandbox_prefix)
        elif func == 'cancel':
            mode = 'cancel'
            return self.render(mode=mode, sandboxPrefix=self.sandbox_prefix)
        return self.redirect('404.html')
    def post(self):
        firstName = cgi.escape(self.request.get('firstName'))
        lastName = cgi.escape(self.request.get('lastName'))
        birthDate = cgi.escape(self.request.get('birthDate'))
        mode = cgi.escape(self.request.get('mode'))
        if not firstName or not lastName or not birthDate:
            self.render(mode='init', sandboxPrefix=self.sandbox_prefix, error_subscription='All customer data should be filled up')
            return
        if mode == 'init':
            data = {
                  "currencyCode":"EUR",
                  "receiverId[0]":"Service", #shop ids must be known
                  "amount[0]":'1.00',
                  "returnUrl":self.confirmUrl,
                  "cancelUrl":self.cancelUrl
                   }
            data = urllib.urlencode(data)
#            req = urllib2.Request(self.payUrl, data)    #due to GAE bug with SSL certs, use urlfetch
            res = ''        
            try:
#                response = urllib2.urlopen(req)
#                res = response.read()
                response = urlfetch.fetch(self.payUrl, method='POST', payload=data, validate_certificate=False)
                res = response.content
            except Exception, e:
                logging.error("%s subscribe init error: %s"%(self.request.path, e.message))           
            return self.response.out.write(res)
        elif mode == 'confirm':
            ppLogin = cgi.escape(self.request.get('ppLogin'))
            customerId = ppLogin.replace('@','_')
            data = {
                  "customerId":customerId,
                  "ppLogin":ppLogin
                   }
            data = urllib.urlencode(data)
            res = ''        
            try:
                response = urlfetch.fetch(self.newCustomerUrl, method='POST', payload=data, validate_certificate=False)
                res = response.content
            except Exception, e:
                logging.error("%s subscribe confirm error1: %s"%(self.request.path, e.message))
            res = dict(urllib2.urlparse.parse_qsl(res))
            if res['result'] != 'success':            
                return self.response.out.write(res)
            time.sleep(3)
            data = {
                  "currencyCode":"EUR",
                  "customerId":customerId,
                  "returnUrl":self.confirmUrl,
                  "cancelUrl":self.cancelUrl
                   }
            data = urllib.urlencode(data)
            res = ''        
            try:
                response = urlfetch.fetch(self.newPreapprovalUrl, method='POST', payload=data, validate_certificate=False)
                res = response.content
            except Exception, e:
                logging.error("%s confirm error: %s"%(self.request.path, e.message))           
            return self.response.out.write(res)

        elif mode == 'cancel':
            return self.redirect(self.cancelledUrl)
        return self.redirect('404.html')    
