from loginhandler import LoginHandler
from hellohandler import HelloPage
from accountpaymenthandler import AccountPaymentHandler
from demosubscriptionhandler import DemoSubscriptionHandler
import webapp2


app = webapp2.WSGIApplication([(r'(?:/sandbox)?/', HelloPage)
                              ,(r'(?:/sandbox)?/login/*', LoginHandler)
                              ,(r'(?:/sandbox)?/accountpayment/(completed)/*', AccountPaymentHandler)
                              ,(r'(?:/sandbox)?/accountpayment/(cancelled)/*', AccountPaymentHandler)
                              ,(r'(?:/sandbox)?/accountpayment/(customer/create)/*', AccountPaymentHandler)
                              ,(r'(?:/sandbox)?/accountpayment/(customer/delete)/*', AccountPaymentHandler)
                              ,(r'(?:/sandbox)?/accountpayment/(preapproval/create)/*', AccountPaymentHandler)
                              ,(r'(?:/sandbox)?/accountpayment/(preapproval/delete)/*', AccountPaymentHandler)
                              ,(r'(?:/sandbox)?/accountpayment/(preapproval/onreturn)/*', AccountPaymentHandler)
                              ,(r'(?:/sandbox)?/accountpayment/(preapproval/oncancel)/*', AccountPaymentHandler)
                              ,(r'(?:/sandbox)?/accountpayment/(preapproval/onipnnotification)/*', AccountPaymentHandler)
                              ,(r'(?:/sandbox)?/accountpayment/(commit)/*', AccountPaymentHandler)
                              ,(r'(?:/sandbox)?/accountpayment/(refund)/*', AccountPaymentHandler)
                              ,(r'(?:/sandbox)?/accountpayment/(commit/onreturn)/*', AccountPaymentHandler)
                              ,(r'(?:/sandbox)?/accountpayment/(commit/oncancel)/*', AccountPaymentHandler)
                              ,(r'(?:/sandbox)?/accountpayment/(commit/onipnnotification)/*', AccountPaymentHandler)
                              ,(r'(?:/sandbox)?/accountpayment/*', AccountPaymentHandler)
                              ,(r'(?:/sandbox)?/subscribe/*', DemoSubscriptionHandler)
                              ,(r'(?:/sandbox)?/subscribe/(confirm)/*', DemoSubscriptionHandler)
                              ,(r'(?:/sandbox)?/subscribe/(cancel)/*', DemoSubscriptionHandler)
                              ],
                              debug=True)

app.run()
