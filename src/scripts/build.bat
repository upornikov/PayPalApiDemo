@echo off
pushd ..
rmdir /s /q ..\build >nul 2>&1
mkdir ..\build || exit /b 1
xcopy /s/y/i .\static ..\build\static || exit /b 1
xcopy /s/y/i .\templates ..\build\templates || exit /b 1
copy /y app.yaml ..\build\ || exit /b 1
copy /y *.py ..\build\ || exit /b 1
popd
exit /b 0
