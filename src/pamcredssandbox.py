'''PAM credentials for setting up sandbox PAM application'''

adaptivePaymentsApiRootUrl = 'https://svcs.sandbox.paypal.com/AdaptivePayments/' 
pamApiHandlerId = 'upornikov_api1.gmail.com'
pamApiHandlerPwd = 'XXXXXXXXXXXXXXXX'
pamApiHandlerSig = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
pamApiHandlerAppId = 'APP-80W284485P519543T'
preapproveAuthRedirect = 'https://sandbox.paypal.com/webscr?cmd=_ap-preapproval&preapprovalkey=%s'
payExecRedirect = 'https://sandbox.paypal.com/webscr?cmd=_ap-payment&paykey=%s'
