import os, jinja2, cgi
from google.appengine.ext import db
from pambase import TheSecretHandler, TheRequestHandler

template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir), autoescape = True)


class LoginHandler(TheRequestHandler):
    def update_default_user(self):
        '''Create/update test user'''
        user = db.GqlQuery('SELECT * FROM %s WHERE username = :1'%self.userDb.__name__, 'testhost').get()
        if not user:
            self.userDb(username='testhost', passwordhash=TheSecretHandler.make_password_hash('BeMyGuest')).put()
        return
    def initialize(self, *a, **kw):
        super(LoginHandler, self).initialize(*a, **kw)
        if self.sandboxed:   #create default POS operator for sandbox tests; pretty expensive operation - disable after creating default user, if performance matters
            self.update_default_user()
    def render(self, **kwargs):
        t = jinja_env.get_template('login.html')
        self.response.out.write(t.render(kwargs))
    def get_user(self, username, password):
        user = db.GqlQuery("SELECT * FROM %s WHERE username = :1"%self.userDb.__name__, username).get()
        if not user:
            return None
        if not TheSecretHandler.check_password(password, user.passwordhash):
            return None
        return user
    def get(self):
        self.render()
    def post(self):
        username = cgi.escape(self.request.get('username'))
        password = cgi.escape(self.request.get('password'))
        user = self.get_user(username, password)
        if not user:
            self.render(username='', error_login='Invalid login')
            return
        self.set_secure_cookie('user_id', TheSecretHandler.make_secure_cookie(user.key().id()), 5)
        self.redirect('%s/'%self.sandbox_prefix)
