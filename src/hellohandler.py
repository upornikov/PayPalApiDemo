from pambase import TheRequestHandler

class HelloPage(TheRequestHandler):
    def get(self):
        self.redirect('%s/accountpayment'%self.sandbox_prefix)
        