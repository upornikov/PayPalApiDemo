from google.appengine.ext import db

'''PAM sandbox data structures'''

class SandboxUser(db.Model):
    '''POS operator sandbox data class'''
    username = db.StringProperty(required = True)
    passwordhash = db.StringProperty(required = True)
    created = db.DateTimeProperty(auto_now_add = True)

class SandboxCustomer(db.Model):
    '''Service customer sandbox data class'''
    nick = db.StringProperty(required = True)
    ppLogin = db.StringProperty(required = True)
    preapproval = db.StringProperty(required = False)
    
class SandboxLedger(db.Model):
    '''Service ledger sandbox data class'''
    paymentId = db.StringProperty(required = True)
    senderId = db.StringProperty(required = True)
    senderEmail = db.StringProperty(required = True)
    payKey = db.StringProperty(required = True)
    currencyCode = db.StringProperty(required = True)
    created = db.DateTimeProperty(auto_now_add = True)
