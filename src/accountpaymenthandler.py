from pambase import TheSecretHandler, TheRequestHandler
from ppadaptivepaymentprocessor import PayPalAdaptivePaymentsProcessor
import urllib, json, cgi, random, string, datetime
import os, jinja2, logging
from google.appengine.ext import db
from google.appengine.api import memcache


template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir), autoescape = True)


class AccountPaymentHandler(TheRequestHandler):
    '''Handler implements PayPal account payment PAM API'''
    def render(self, **kwargs):
        t = jinja_env.get_template('accountpaymentview.html')
        self.response.out.write(t.render(kwargs))
    def setupRedirects(self):
        '''Import paypal redirect urls'''
        if self.sandboxed:
            from pamcredssandbox import preapproveAuthRedirect, payExecRedirect
        else:            
            from pamcreds import preapproveAuthRedirect, payExecRedirect
        self.preapproveAuthRedirect = preapproveAuthRedirect
        self.payExecRedirect = payExecRedirect
    def __init__(self, request, response):
        self.initialize(request, response)
        self.setupRedirects()
        #default redirects, normally PAM client should specify them in the call parameters
        self.hostUrl = self.request.host_url + '%s/accountpayment'%self.sandbox_prefix
        self.cancelledUrl = self.hostUrl + '/cancelled'
        self.completedUrl = self.hostUrl + '/completed'
        self.ppaccountprocessor = PayPalAdaptivePaymentsProcessor(hostUrl=self.hostUrl)
    def customerExists(self, nick):
        return db.GqlQuery("SELECT __key__ FROM %s WHERE nick = :1"%self.customerDb.__name__, nick).get() != None
    def getCustomer(self, nick):
        return db.GqlQuery("SELECT * FROM %s WHERE nick = :1"%self.customerDb.__name__, nick).get()
    def getPayment(self, paymentId):
        return db.GqlQuery("SELECT * FROM %s WHERE paymentId = :1"%self.ledgerDb.__name__, paymentId).get()
    def updateLedger(self, paymentId, payment):
        rec = db.GqlQuery("SELECT * FROM %s WHERE paymentId = :1"%self.ledgerDb.__name__, paymentId).get()
        try:
            if not rec:
                rec = self.ledgerDb(paymentId=paymentId, senderId=payment['senderId'], senderEmail=payment['senderEmail'], payKey=payment['payKey'], currencyCode=payment['currencyCode'])
            else:
                rec.senderId=payment['senderId']
                rec.senderEmail=payment['senderEmail']
                rec.payKey=payment['payKey']
                rec.currencyCode=payment['currencyCode']
            rec.put()
        except Exception, e:
            logging.error("Fail update %s: %s"%(self.ledgerDb.__name__, e.message))
            pass           
    
    def get(self, func = ''):
        '''PAM GET API implementation'''
        if func == '':
            if not self.user: return self.redirect('%s/login'%self.sandbox_prefix)
            self.set_secure_cookie('user_id', TheSecretHandler.make_secure_cookie(self.user.key().id()), 5)
            self.request.headers['Content-Type'] = 'text/html'
            return self.render(sandboxPrefix=self.sandbox_prefix)
        elif func == 'refund':
            if self.user: self.set_secure_cookie('user_id', TheSecretHandler.make_secure_cookie(self.user.key().id()), 5)
            return self.response.out.write(urllib.urlencode(self.doRefund()))
        elif func == "completed":
            return self.response.out.write('Operation complete')
        elif func == "cancelled":
            return self.response.out.write('Operation was cancelled')
        elif func == 'preapproval/onreturn':
            returnUrl = str(cgi.escape(self.request.get('url', self.completedUrl)))
            preapprovalId = str(cgi.escape(self.request.get('id')))
            preapproval = memcache.get(preapprovalId)
            try:
                customer = self.getCustomer(preapproval['nick'])
                del preapproval['returnUrl']
                del preapproval['cancelUrl']
                del preapproval['ipnNotificationUrl']
                customer.preapproval = json.dumps(preapproval)
                customer.put()
                memcache.delete(preapprovalId)
            except Exception, e:
                logging.error("Fail storing preapproval: %s"%e.message)
                try: self.ppaccountprocessor.cancel_preapproval(preapproval['key'])
                except: pass                
            return self.redirect(returnUrl)    
        elif func == 'preapproval/oncancel':
            returnUrl = str(cgi.escape(self.request.get('url', self.cancelledUrl)))
            preapprovalId = str(cgi.escape(self.request.get('id')))
            preapproval = memcache.get(preapprovalId)
            try:
                memcache.delete(preapprovalId)
                self.ppaccountprocessor.cancel_preapproval(preapproval['key'])
            except:
                pass
            return self.redirect(returnUrl)
        elif func == 'preapproval/onipnnotification':
            returnUrl = str(cgi.escape(self.request.get('url', self.completedUrl)))
            return self.redirect(returnUrl)
        elif func == 'commit/onreturn':
            returnUrl = str(cgi.escape(self.request.get('url', self.completedUrl), self))
            paymentId = str(cgi.escape(self.request.get('id')))
            payment = memcache.get(paymentId)
            self.updateLedger(paymentId, payment)
            try: memcache.delete(paymentId)
            except: pass
            return self.redirect(returnUrl)
        elif func == 'commit/oncancel':
            returnUrl = str(cgi.escape(self.request.get('url', self.cancelledUrl)))
            paymentId = str(cgi.escape(self.request.get('id')))
            payment = memcache.get(paymentId)
            try: memcache.delete(paymentId)
            except: pass
            return self.redirect(returnUrl)
        elif func == 'commit/onipnnotification':
            returnUrl = str(cgi.escape(self.request.get('url', self.completedUrl)))
            paymentId = str(cgi.escape(self.request.get('id')))
            payment = memcache.get(paymentId)
            return self.redirect(returnUrl)
        else:
            return self.redirect('404.html')

    def setPreapprovalData(self, customer):
        data = {}
        pinRequired = cgi.escape(self.request.get('pinRequired'))
        if pinRequired and str(pinRequired)!='0' and str(pinRequired).lower()!='no' and str(pinRequired).lower()!='false' and str(pinRequired).lower()!='not_required':
            data['pinType'] = 'REQUIRED'
        else:
            data['pinType'] = 'NOT_REQUIRED'
        maxNumberOfPayments = cgi.escape(self.request.get('maxNumberOfPayments'))
        if maxNumberOfPayments:
            data['maxNumberOfPayments'] = int(maxNumberOfPayments)
        maxAmountPerPayment = cgi.escape(self.request.get('maxAmountPerPayment'))
        if maxAmountPerPayment:
            data['maxAmountPerPayment'] = int(maxAmountPerPayment)
        try:
            startingDate = datetime.datetime.strptime(cgi.escape(self.request.get('startingDate')), "%Y-%m-%dZ")
        except:    
            startingDate = datetime.datetime.utcnow()
        data.update({
                    'nick': customer.nick,
                    'senderEmail': customer.ppLogin,
                    'currencyCode': cgi.escape(self.request.get('currencyCode', 'EUR')),
                    'pinRequired': pinRequired,
                    'startingDate': startingDate.strftime("%Y-%m-%dZ"),
                    'endingDate': cgi.escape(self.request.get('endingDate', (startingDate + datetime.timedelta(days=365)).strftime("%Y-%m-%dZ"))),
                    'maxTotalAmountOfAllPayments': int(cgi.escape(self.request.get('maxTotalAmountOfAllPayments', '1400'))),
                    'cancelUrl': cgi.escape(self.request.get('cancelUrl', self.cancelledUrl)), 
                    'returnUrl': cgi.escape(self.request.get('returnUrl', self.completedUrl)), 
                    'ipnNotificationUrl': cgi.escape(self.request.get('ipnNotificationUrl', self.completedUrl))
                    })
        return data
    def setFailResult(self, *arg, **kwarg):
        res = {}
        res['result'] = 'fail'
        res['messages'] = []
        res['messages'].extend(arg)
        res.update(kwarg)
        return res
    def setSuccessResult(self, *arg, **kwarg):
        res = {}
        res['result'] = 'success'
        res['messages'] = []
        res['messages'].extend(arg)
        res.update(kwarg)
        return res
    def doRefund(self):
        #paymentId, [amount]
        try:
            data = {}
            payment = self.getPayment(cgi.escape(self.request.get('paymentId')))
            data['payKey'] = payment.payKey
            amount =  cgi.escape(self.request.get('amount'))
            if amount:
                data['currencyCode'] = payment.currencyCode
                receiverList = [{'email':payment.senderEmail, 'amount': amount},]
                data.update(self.ppaccountprocessor.ReceiverList(*receiverList).flatten())
            res = self.ppaccountprocessor.do_refund(**data)
            if res.get('responseEnvelope.ack') == 'Success' and res.get('refundInfoList.refundInfo(0).refundTransactionStatus') == 'COMPLETED':
                s = self.setSuccessResult("payment %s refund %s success"%(payment.paymentId, res.get('refundInfoList.refundInfo(0).refundGrossAmount')))
            else:
                s = ["payment %s refund failure"%payment.paymentId]
                for s1 in ['error(0).message', 'refundInfoList.refundInfo(0).refundStatus']: 
                    if res.get(s1): s.append(res.get(s1))
                s = self.setFailResult(*s)
            return s                    
        except Exception, e:
            return self.setFailResult("refund failed: %s"%e.message)
    
    def post(self, func = ''):
        '''POST handler for corresponding API function'''
        if self.user: self.set_secure_cookie('user_id', TheSecretHandler.make_secure_cookie(self.user.key().id()), 5)
        if func == 'customer/create':
            nick = cgi.escape(self.request.get('customerId'))
            ppLogin = cgi.escape(self.request.get('ppLogin'))
            if not nick or not ppLogin:
                return self.response.out.write(urllib.urlencode(self.setFailResult("customerId and ppLogin can not be empty")))
            if self.customerExists(nick):
                return self.response.out.write(urllib.urlencode(self.setFailResult("customer %s already exists"%nick)))
            c = self.customerDb(nick = nick, ppLogin = ppLogin, preapproval = None)
            c.put()
            return self.response.out.write(urllib.urlencode(self.setSuccessResult("customer %s was created"%nick)))
        elif func == 'customer/delete':
            nick = cgi.escape(self.request.get('customerId'))
            try:
                self.getCustomer(nick).delete()
                res = self.setSuccessResult("customer %s was deleted"%nick)
            except:
                res = self.setSuccessResult("customer %s not found"%nick)
            return self.response.out.write(urllib.urlencode(res))
        if func == 'preapproval/create':
            nick = cgi.escape(self.request.get('customerId'))
            customer = self.getCustomer(nick)
            if not customer:
                return self.response.out.write(urllib.urlencode(self.setFailResult("no customer %s"%nick)))
            if customer.preapproval:
                return self.response.out.write(urllib.urlencode(self.setFailResult("preapproval from customer %s already exists"%nick)))
            try:
                preapproval = self.setPreapprovalData(customer)
                preapprovalId = ''.join(random.choice(string.letters) for i in range(0,16))
                res = self.ppaccountprocessor.create_preapproval(preapprovalId=preapprovalId, **preapproval)
                if res['responseEnvelope.ack'][:7] != 'Success':  #(Failure, FailureWithWarning, Success, SuccessWithWarning)
                    messages = []
                    for i in range(0, 20):
                        message = 'error(%d).message'%i
                        if message in res:
                            messages.append(res[message])
                        else:
                            break
                    return self.response.out.write(urllib.urlencode(self.setFailResult("preapproval from customer %s failed"%nick, *messages)))
                preapproval['key'] = res['preapprovalKey']        #if failed, exception here
                memcache.set(preapprovalId, preapproval)                  #save preapproval in cache for handling in redirections
                return self.redirect(self.preapproveAuthRedirect%preapproval['key'])
            except Exception, e:
                return self.response.out.write(urllib.urlencode(self.setFailResult("preapproval from customer %s failed: %s"%(nick, e.message))))
        elif func == 'preapproval/delete':
            nick = cgi.escape(self.request.get('customerId'))
            customer = self.getCustomer(nick)
            try:
                try:
                    preapproval = json.loads(customer.preapproval)
                    self.ppaccountprocessor.cancel_preapproval(preapproval['key'])
                except: pass
                customer.preapproval = None
                customer.put()
                res = self.setSuccessResult("delete preapproval from customer %s success"%nick)
            except Exception, e:
                res = self.setFailResult("delete preapproval from customer %s failed: %s"%(nick, e.message))
            return self.response.out.write(urllib.urlencode(res))
        elif func == 'commit':
            #sender, [receiver[i], amount[i]:0, primary[i]:false], feesPayer:EACHRECEIVER(SENDER, EACHRECEIVER, PRIMARYRECEIVER, SECONDARYONLY)
            #currencyCode:(EUR,USD,...), pin:'', memo:'', 
            senderNick = cgi.escape(self.request.get('senderId'))
            senderEmail = None
            senderPreapproval = None
            if senderNick:
                sender = self.getCustomer(senderNick)
                if not sender:
                    return self.response.out.write(urllib.urlencode(self.setFailResult("payment failed: sender %s is not registered"%senderNick)))
                senderEmail = sender.ppLogin
                senderPreapproval = sender.preapproval
            receiverList = []
            i = 0
            while i < 6:    #paypal: 6 receivers max
                nick = cgi.escape(self.request.get('receiverId[%d]'%i))
                if not nick:
                    i += 1
                    continue
                receiver = self.getCustomer(nick)
                if not receiver:
                    return self.response.out.write(urllib.urlencode(self.setFailResult("payment failed: receiver %s is not registered"%nick)))
                amount = cgi.escape(self.request.get('amount[%d]'%i), '0')
                primary = cgi.escape(self.request.get('primary[%d]'%i, 'false'))
                receiverList.append({'email':receiver.ppLogin, 'amount':amount, 'primary':primary})                        
                i += 1
            payment = {'senderId':senderNick,
                       'senderEmail':senderEmail,
                       'currencyCode':cgi.escape(self.request.get('currencyCode')),
                       'feesPayer':cgi.escape(self.request.get('feesPayer', 'EACHRECEIVER')),
                       'memo':cgi.escape(self.request.get('memo')), 
                       'pin':cgi.escape(self.request.get('pin')),
                       'cancelUrl':cgi.escape(self.request.get('cancelUrl', self.cancelledUrl)),
                       'returnUrl':cgi.escape(self.request.get('returnUrl', self.completedUrl)),
                       'ipnNotificationUrl':cgi.escape(self.request.get('ipnNotificationUrl', self.completedUrl))
                      }
            if senderPreapproval:
                preapproval = json.loads(senderPreapproval)
                try: 
                    payment['preapprovalKey'] = preapproval['key']
                except: 
                    return self.response.out.write(urllib.urlencode(self.setFailResult("payment failed: broken preapproval from customer %s"%senderNick)))
            payment['receiverList'] = self.ppaccountprocessor.ReceiverList(*receiverList).flatten()
            paymentId = ''.join(random.choice(string.letters) for i in range(0,16))
            payment['paymentId'] = paymentId
            res = self.ppaccountprocessor.create_payment(**payment)
            try:
                if res['responseEnvelope.ack'][:7] != 'Success':  #(Failure, FailureWithWarning, Success, SuccessWithWarning)
                    messages = []
                    for i in range(0, 20):
                        message = 'error(%d).message'%i
                        if message in res:
                            messages.append(res[message])
                        else:
                            break
                    return self.response.out.write(urllib.urlencode(self.setFailResult(*messages)))
                elif res['paymentExecStatus'] == 'CREATED':
                    payment['payKey'] = res['payKey']
                    memcache.set(paymentId, payment)     #save payment in cache for handling in redirections
                    return self.redirect(self.payExecRedirect%res['payKey'])
                elif res['paymentExecStatus'] == 'COMPLETED': 
                    payment['payKey'] = res['payKey']
                    self.updateLedger(paymentId, payment)
                    r = self.setSuccessResult('payment completed', {'paymentId': paymentId, 'preapprovalExpire':preapproval['endingDate']})
                    return self.response.out.write(urllib.urlencode(r))
                else:
                    return self.response.out.write(urllib.urlencode('Payment failed or pending. Failed transfers will be reversed.'))
            except Exception, e:
                return self.response.out.write(urllib.urlencode(self.setFailResult("payment failed; unexpected error: %s"%e.message)))
        elif func == 'refund':
            return self.response.out.write(urllib.urlencode(self.doRefund()))
        else:
            return self.redirect('/404.html')
